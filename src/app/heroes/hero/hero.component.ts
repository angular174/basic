import { Component } from '@angular/core';

@Component({
  selector: 'app-heroes-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css'],
})
export class HeroComponent {
  public name: string = 'pepe';
  public year: number = 20;

  get nameCapitalize(): string {
    return this.name.toUpperCase();
  }

  public getHeroDescription() {
    return `${this.name} - ${this.year}`;
  }

  public changeName(): void {
    this.name = 'Naruto';
  }

  public changeYear(): void {
    this.year = 22;
  }
}
