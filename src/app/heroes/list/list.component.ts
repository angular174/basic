import { Component } from '@angular/core';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent {
  public heroNames: string[] = ['a1', 'a2', 'a3', 'a4', 'a5'];
  public deleteHero?: string = '';
  public deleteHeroNames(): void {
    this.deleteHero = this.heroNames.pop();
  }
}
